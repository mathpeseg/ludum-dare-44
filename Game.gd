extends Control
#William Schneider
#Leonardo Dias Leffa
#Matheus Pesegoginski
#Henrique 

#2 Menus faltantes, 1 Créditos, 1 Seleção de numero de jogadores.
onready var TimerNode = $Timer
onready var timelabel:Node

func _ready():
	if has_node("Map"):
		timelabel= $Map/TimeLabel
	elif has_node("Map2"):
		timelabel= $Map2/TimeLabel
		
func _process(delta):
	if has_node("Map"):
		timelabel = get_node("Map/TimeLabel")
	elif has_node("Map2"):
		timelabel = get_node("Map2/TimeLabel")
	else:
		timelabel = null
		
	if timelabel != null :
		timelabel.text = "Time: " + str(int(TimerNode.time_left))
	
func game_over():
	get_node("Map").queue_free()
	get_node("Points").add_child(get_node("Map/Skull/PointsLabel").duplicate())
	get_node("Points").add_child(get_node("Map/Skull2/PointsLabel").duplicate())
	
	if has_node("Map/Skull3"):
		get_node("Points").add_child(get_node("Map/Skull3/PointsLabel").duplicate())
	if has_node("Map/Skull4"):
		get_node("Points").add_child(get_node("Map/Skull4/PointsLabel").duplicate())
		
	var gameover = load("res://GameOver.tscn")
	self.add_child(gameover.instance())

func _on_Play__pressed():
	TimerNode.start()
	print('teste')
