extends Node2D

var xArenaStart:float
var xArenaEnd:float
var yArenaStart:float
var yArenaEnd:float
var time:float
var picked = false
var is_inside_area = false
var sprite_h_size = 16
var picker: Node2D
enum P_TYPE  { Alpha, Beta, Gama, Delta }
export (P_TYPE) onready var person_type  = P_TYPE.Alpha


onready var ArenaNode:Node2D = get_parent()
onready var TweenNode = $Tween
onready var new_position: Vector2 setget , get_new_position

var pontos: int
var tipo: int

onready var spritenode = get_node("Sprite")
var red_sprite = load("res://Person/red-sheet.png")
var green_sprite = load("res://Person/green-sheet.png")
var blue_sprite = load("res://Person/blue-sheet.png")
var white_sprite = load("res://Person/rainbow-sheet.png")

func _ready():
	randomize()
	self.person_type = randi() % 4
	
	
	tipo = randi() % 10
	if tipo < 1:
		spritenode.set_texture(white_sprite)
		pontos = 130
	elif tipo < 3:
		spritenode.set_texture(blue_sprite)
		pontos = 80
	elif tipo < 6:
		spritenode.set_texture(green_sprite)
		pontos = 50
	elif tipo < 10:
		spritenode.set_texture(red_sprite)
		pontos = 30
	
	
	xArenaStart = ArenaNode.global_position.x + sprite_h_size/2
	yArenaStart = ArenaNode.global_position.y + sprite_h_size/2
	xArenaEnd = xArenaStart + ArenaNode.scale.x - sprite_h_size/2
	yArenaEnd = yArenaStart + ArenaNode.scale.y - sprite_h_size/2
	self.global_position.x = rand_range(xArenaStart,xArenaEnd)
	self.global_position.y = rand_range(yArenaStart,yArenaEnd)
	movement()


func movement():
	if picked == false:
		time = rand_range(3, 5)
		TweenNode.interpolate_property(self, "global_position", self.global_position,  self.new_position, time, Tween.TRANS_LINEAR, Tween.EASE_IN)
		TweenNode.start()


func get_new_position():
	new_position.x = rand_range(xArenaStart,xArenaEnd)
	new_position.y = rand_range(yArenaStart,yArenaEnd)
	
	return new_position


func _process(_delta):
	if is_inside_area:
		self.global_position = picker.global_position
		print(picker.global_position)
		picked = true
		
	if tipo < 1:
		spritenode.set_texture(white_sprite)
		pontos = 130
	elif tipo < 3:
		spritenode.set_texture(blue_sprite)
		pontos = 80
	elif tipo < 6:
		spritenode.set_texture(green_sprite)
		pontos = 50
	elif tipo < 10:
		spritenode.set_texture(red_sprite)
		pontos = 30
		


func _on_Tween_tween_completed(_object, _key):
	if picked == false:
		time = rand_range(3, 5)
		TweenNode.interpolate_property(self, "global_position", self.global_position, self.new_position, time, Tween.TRANS_LINEAR, Tween.EASE_IN)
		TweenNode.start()
		
func _set_pontos():
	
	tipo = randi() % 10
	
	pass
